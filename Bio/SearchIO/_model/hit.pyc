ó
«?ac           @  sÂ   d  Z  d d l m Z d d l m Z d d l m Z d d l m Z m	 Z	 d d l
 m Z m Z d d l m Z d d	 l m Z d
 e f d     YZ e d k r¾ d d l m Z e   n  d S(   s3   Bio.SearchIO object to model a single database hit.iÿÿÿÿ(   t   print_function(   t   chain(   t   filter(   t   getattr_strt   trim_str(   t   allitemst   optionalcascadei   (   t   _BaseSearchObject(   t   HSPt   Hitc           B  s4  e  Z d  Z d# Z g  d$ d$ d  Z d   Z d   Z d   Z d   Z	 e	 Z
 d   Z d   Z d	   Z d
   Z d   Z d   Z e d d d  Z e d d d  Z e d d d  Z e d d d  Z e d d  Z e d    Z d   Z d$ d  Z d   Z d$ d  Z d  d!  Z d$ e e  d"  Z! RS(%   s²  Class representing a single database hit of a search result.

    Hit objects are the second-level container in the SearchIO module. They
    are the objects contained within a QueryResult (see QueryResult). They
    themselves are container for HSP objects and will contain at least one
    HSP.

    To have a quick look at a Hit and its contents, invoke `print` on it:

    >>> from Bio import SearchIO
    >>> qresult = next(SearchIO.parse('Blast/mirna.xml', 'blast-xml'))
    >>> hit = qresult[3]
    >>> print(hit)
    Query: 33211
           mir_1
      Hit: gi|301171322|ref|NR_035857.1| (86)
           Pan troglodytes microRNA mir-520c (MIR520C), microRNA
     HSPs: ----  --------  ---------  ------  ---------------  ---------------------
              #   E-value  Bit score    Span      Query range              Hit range
           ----  --------  ---------  ------  ---------------  ---------------------
              0   8.9e-20     100.47      60           [1:61]                [13:73]
              1   3.3e-06      55.39      60           [0:60]                [13:73]

    You can invoke `len` on a Hit object to see how many HSP objects it contains:

    >>> len(hit)
    2

    Hit objects behave very similar to Python lists. You can retrieve the HSP
    object inside a Hit using the HSP's integer index. Hit objects can also be
    sliced, which will return a new Hit objects containing only the sliced HSPs:

    # HSP items inside the Hit can be retrieved using its integer index
    >>> hit[0]
    HSP(hit_id='gi|301171322|ref|NR_035857.1|', query_id='33211', 1 fragments)

    # slicing returns a new Hit
    >>> hit
    Hit(id='gi|301171322|ref|NR_035857.1|', query_id='33211', 2 hsps)
    >>> hit[:1]
    Hit(id='gi|301171322|ref|NR_035857.1|', query_id='33211', 1 hsps)
    >>> print(hit[1:])
    Query: 33211
           mir_1
      Hit: gi|301171322|ref|NR_035857.1| (86)
           Pan troglodytes microRNA mir-520c (MIR520C), microRNA
     HSPs: ----  --------  ---------  ------  ---------------  ---------------------
              #   E-value  Bit score    Span      Query range              Hit range
           ----  --------  ---------  ------  ---------------  ---------------------
              0   3.3e-06      55.39      60           [0:60]                [13:73]

    Hit objects provide `filter` and `map` methods, which are analogous to
    Python's built-in `filter` and `map` except that they return a new Hit
    object instead of a list.

    Here is an example of using `filter` to select for HSPs whose e-value is
    less than 1e-10:

    >>> evalue_filter = lambda hsp: hsp.evalue < 1e-10
    >>> filtered_hit = hit.filter(evalue_filter)
    >>> len(hit)
    2
    >>> len(filtered_hit)
    1
    >>> print(filtered_hit)
    Query: 33211
           mir_1
      Hit: gi|301171322|ref|NR_035857.1| (86)
           Pan troglodytes microRNA mir-520c (MIR520C), microRNA
     HSPs: ----  --------  ---------  ------  ---------------  ---------------------
              #   E-value  Bit score    Span      Query range              Hit range
           ----  --------  ---------  ------  ---------------  ---------------------
              0   8.9e-20     100.47      60           [1:61]                [13:73]

    There are also other methods which are counterparts of Python lists' methods
    with the same names: `append`, `index`, `pop`, and `sort`. Consult their
    respective documentations for more details and examples of their usage.

    t   _itemsc           s¨   | |  _  | |  _ d |  _ d |  _ xI d	 D]A   t t   f d   | D   d k r+ t d     q+ q+ Wg  |  _ x( | D]  } |  j	 |  |  j
 |  q Wd S(
   sn  Initializes a Hit object.

        Arguments:
        hsps -- List containing HSP objects.
        id -- String of the Hit ID
        query_id -- String of the Hit's query ID

        If multiple HSP objects are used for initialization, they must all
        have the same `query_id`, `query_description`, `hit_id`, and
        `hit_description` properties.
        t   query_idt   query_descriptiont   hit_idt   hit_descriptionc         3  s   |  ] } t  |    Vq d  S(   N(   t   getattr(   t   .0t   hsp(   t   attr(    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pys	   <genexpr>   s    i   s6   Hit object can not contain HSPs with more than one %s.N(   R   R   R   R   (   t   _idt	   _query_idt   Nonet   _descriptiont   _query_descriptiont   lent   sett
   ValueErrorR
   t   _validate_hspt   append(   t   selft   hspst   idR   R   (    (   R   s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __init__k   s    				
(	c         C  s   d |  j  |  j t |   f S(   Ns    Hit(id=%r, query_id=%r, %r hsps)(   R   R   R   (   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __repr__   s    c         C  s   t  |  j  S(   N(   t   iterR   (   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __iter__   s    c         C  s   t  |  j  S(   N(   R   R   (   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __len__   s    c         C  s   t  |  j  S(   N(   t   boolR   (   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __bool__   s    c         C  s   | |  j  k S(   N(   R
   (   R   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __contains__   s    c      	   C  s<  g  } d |  j  } |  j r< | t d |  j d d  7} n  | j |  d |  j } t |  d  ry | d |  j 7} n  |  j r¢ | t d |  j d d  7} n  | j |  |  j sÈ | j d  ng| j d	 d% d& d' d( d) d
 d f  d } | j | d*  | j | d+ d, d- d. d/ d
 d f  xÿ t	 |  j  D]î \ } } t
 | d d d } t
 | d d d } t
 | d  }	 t
 | d  }
 t
 | d  } d  |
 | f } t | d d!  } t
 | d"  } t
 | d#  } d  | | f } t | d d!  } | j | t |  | | |	 | | f  q=Wd$ j |  S(0   Ns	   Query: %ss
   
       %siP   s   ...s	     Hit: %st   seq_lens    (%i)s    HSPs: ?s    HSPs: %s  %s  %s  %s  %s  %st   -i   i   i	   i   i   i   s   %11s  %8s  %9s  %6s  %15s  %21st   #s   E-values	   Bit scoret   Spans   Query ranges	   Hit ranget   evaluet   fmts   %.2gt   bitscores   %.2ft   aln_spant   query_startt	   query_ends   [%s:%s]s   ~]t	   hit_startt   hit_ends   
s   ----s   --------s	   ---------s   ------s   ---------------(   R*   s   E-values	   Bit scoreR+   s   Query ranges	   Hit ranges   ----s   --------s	   ---------s   ------s   ---------------(   R   R   R   R   R   t   hasattrR(   t   descriptionR   t	   enumerateR   t   strt   join(   R   t   linest   qid_linet   hid_linet   patternt   idxR   R,   R.   R/   R0   R1   t   query_rangeR2   R3   t	   hit_range(    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __str__¢   sH    						'c         C  sA   t  | t  r6 |  j |  j |  } |  j |  | S|  j | S(   N(   t
   isinstancet   slicet	   __class__R   t   _transfer_attrsR
   (   R   R=   t   obj(    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __getitem__×   s
    c         C  sT   t  | t t f  r6 x+ | D] } |  j |  q Wn |  j |  | |  j | <d  S(   N(   RA   t   listt   tupleR   R
   (   R   R=   R   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __setitem__ß   s
    c         C  s   |  j  | =d  S(   N(   R
   (   R   R=   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   __delitem__é   s    c         C  sj  t  | t  s t d   n  |  j rf|  j d k	 rj | j |  j k rv t d |  j | j f   qv n | j |  _ |  j d k	 r¹ | j	 |  j k rÅ t d |  j | j	 f   qÅ n | j	 |  _ |  j
 d k	 r| j
 |  j
 k rt d |  j
 | j
 f   qn | j
 |  _
 |  j d k	 rW| j |  j k rct d |  j | j f   qcqf| j |  _ n  d S(   s£   Validates an HSP object.

        Valid HSP objects have the same hit_id as the Hit object ID and the
        same query_id as the Hit object's query_id.

        s)   Hit objects can only contain HSP objects.s.   Expected HSP with hit ID %r, found %r instead.s7   Expected HSP with hit description %r, found %r instead.s0   Expected HSP with query ID %r, found %r instead.s9   Expected HSP with query description %r, found %r instead.N(   RA   R   t	   TypeErrorR
   R   R   R   R   R5   R   R   R   (   R   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyR   í   s2    	R   R   s   Hit descriptionR   R   s.   Description of the query that produced the hitR   R   s   Hit ID string.R   R   s,   ID string of the query that produced the hitt   docs    HSP objects contained in the Hitc         C  s    g  t  |  j   D] } | ^ q S(   s(   HSPFragment objects contained in the Hit(   R   R
   (   R   t   frag(    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt	   fragments"  s    c         C  s!   |  j  |  |  j j |  d S(   s)  Adds a HSP object to the end of Hit.

        Parameters
        hsp -- HSP object to append.

        Any HSP object appended must have the same `hit_id` property as the
        Hit object's `id` property and the same `query_id` property as the
        Hit object's `query_id` property.

        N(   R   R
   R   (   R   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyR   (  s    c         C  sB   t  t | |  j   } | r> |  j |  } |  j |  | Sd S(   s]  Creates a new Hit object whose HSP objects pass the filter
        function.

        Arguments:
        func -- Callback function that accepts a HSP object as its parameter,
                does a boolean check, and returns True or False.

        `filter` is analogous to Python's built-in `filter` function, except
        that instead of returning a list it returns a `Hit` object. Here is an
        example of using `filter` to select for HSPs having bitscores bigger
        than 60:

        >>> from Bio import SearchIO
        >>> qresult = next(SearchIO.parse('Blast/mirna.xml', 'blast-xml'))
        >>> hit = qresult[3]
        >>> evalue_filter = lambda hsp: hsp.bitscore > 60
        >>> filtered_hit = hit.filter(evalue_filter)
        >>> len(hit)
        2
        >>> len(filtered_hit)
        1
        >>> print(filtered_hit)
        Query: 33211
               mir_1
          Hit: gi|301171322|ref|NR_035857.1| (86)
               Pan troglodytes microRNA mir-520c (MIR520C), microRNA
         HSPs: ----  --------  ---------  ------  ---------------  ---------------------
                  #   E-value  Bit score    Span      Query range              Hit range
               ----  --------  ---------  ------  ---------------  ---------------------
                  0   8.9e-20     100.47      60           [1:61]                [13:73]

        N(   RG   R   R   RC   RD   (   R   t   funcR   RE   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyR   6  s
    !c         C  s   |  j  j |  S(   s}   Returns the index of a given HSP object, zero-based.

        Arguments:
        hsp -- HSP object to be looked up.

        (   R
   t   index(   R   R   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyRP   ]  s    c         C  sf   | d k	 r2 g  |  j D] } | |  ^ q } n
 |  j } | rb |  j |  } |  j |  | Sd S(   sx  Creates a new Hit object, mapping the given function to its HSPs.

        Arguments:
        func -- Callback function that accepts a HSP object as its parameter and
                also returns a HSP object.

        `map` is analogous to Python's built-in `map` function. It is applied to
        all HSPs contained in the Hit object and returns a new Hit object.

        N(   R   R   RC   RD   (   R   RO   t   xR   RE   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   mapf  s    &
iÿÿÿÿc         C  s   |  j  j |  S(   s¡   Removes and returns the HSP object at the specified index.

        Arguments:
        index -- Integer denoting the index of the HSP object to remove.

        (   R
   t   pop(   R   RP   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyRS   z  s    c         C  sf   | r" |  j  j d | d |  n@ |  j } | j d | d |  |  j |  } |  j |  | Sd S(   s  Sorts the HSP objects.

        Arguments:
        key -- Function used to sort the HSP objects.
        reverse -- Boolean, whether to reverse the sorting or not.
        in_place -- Boolean, whether to perform sorting in place (in the same
                    object) or not (creating a new object).

        `sort` defaults to sorting in-place, to mimick Python's `list.sort`
        method. If you set the `in_place` argument to False, it will treat
        return a new, sorted Hit object and keep the initial one unsorted

        t   keyt   reverseN(   R
   t   sortR   RC   RD   (   R   RT   RU   t   in_placeR   RE   (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyRV     s    
(   R
   N("   t   __name__t
   __module__t   __doc__t   _NON_STICKY_ATTRSR   R    R!   R#   R$   R&   t   __nonzero__R'   R@   RF   RI   RJ   R   R   R5   R   R   R   R   R   t   propertyRN   R   R   RP   RR   RS   t   Falset   TrueRV   (    (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyR	      s<   P#						5		
		*						'			t   __main__(   t   run_doctestN(   RZ   t
   __future__R    t	   itertoolsR   t	   Bio._py3kR   t
   Bio._utilsR   R   t   Bio.SearchIO._utilsR   R   t   _baseR   R   R   R	   RX   Ra   (    (    (    s@   /home/allio/bin/MITOFINDER/MitoFinder/Bio/SearchIO/_model/hit.pyt   <module>   s   ÿ 